#!/bin/bash

cp -r /build/conf/* /etc/cups/

groupadd lpadmin || echo "Group Exists"
useradd ${CUPS_USER:-cups} --system -G root,lpadmin --no-create-home || echo "User Exists"
echo "${CUPS_USER:-cups}:${CUPS_PASSWORD:-cups}" | chpasswd


/usr/sbin/cupsd
sleep 5
python /build/build.py
pkill -15 -f cupsd
# THIS IS REQUIRED TO LET CUPS WRITE FILES BEFORE CLOSING
sleep 10
