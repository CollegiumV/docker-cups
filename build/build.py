#!/usr/bin/env python

__author__ = "Dino Occhialini"
__email__ = "dino.occhialini@gmail.com"

import cups
import yaml


def add_printers(conn, printer_list):
    for printer in printer_list:
        print(printer)
        printer_name = printer['name']
        # Add printer
        conn.addPrinter(printer_name, ppdname=printer['ppd'])
        # Enable all configured printers
        conn.enablePrinter(printer_name)
        # Set the configured uri for each printer
        conn.setPrinterDevice(printer_name, printer['address'])
        # If description is defined, set it
        if 'description' in printer:
            conn.setPrinterInfo(printer_name, printer['description'])
        # If location is defined, set it
        if 'location' in printer:
            conn.setPrinterLocation(printer_name, printer['location'])
        # If shared is defined, set it
        if 'shared' in printer:
            conn.setPrinterShared(printer_name, printer['shared'])
        conn.acceptJobs(printer_name)


def add_classes(conn, class_list):
    for printer_class in class_list:
        print(printer_class)
        class_name = printer_class['name']
        # Classes are added by adding members
        for member in printer_class['members']:
            conn.addPrinterToClass(member, class_name)
        conn.enablePrinter(class_name)
        # Set description if defined
        if 'description' in printer_class:
            conn.setPrinterInfo(class_name, printer_class['description'])
        # Set location if defined
        if 'location' in printer_class:
            conn.setPrinterLocation(class_name, printer_class['location'])
        # Set shared
        conn.setPrinterShared(class_name, printer_class.get('shared', True))
        conn.acceptJobs(class_name)


def set_default(conn, default_printer):
    conn.setDefault(default_printer)


def main():
    conf = yaml.load(file('/config.yml'))
    connection = cups.Connection()
    add_printers(connection, conf['printers'])
    add_classes(connection, conf['classes'])
    if 'default' in conf:
        set_default(connection, conf['default'])


if __name__ == "__main__":
    main()

# vim: ai ts=4 sts=4 et sw=4 ft=python
