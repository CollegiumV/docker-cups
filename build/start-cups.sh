#!/bin/sh

# Author: Dino Occhialini
# Email: dino.occhialini@gmail.com

/usr/sbin/cupsd -f &&

rm /home/papercut/providers/print/linux-x64/*.pid

/home/papercut/providers/print/linux-x64/pc-event-monitor.rc start

# vim: ai ts=2 sw=2 et sts=2 ft=sh
