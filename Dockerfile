FROM fedora:27

LABEL author="cvadmins"
LABEL version="1.0.0"

ARG PAPERCUT_URL=https://cdn.papercut.com/files/pcng/14.x/pcng-setup-14.3-linux-x64.sh
ARG PAPERCUT_SERVER=na.collegiumv.org

ADD build /build

RUN dnf -y update && dnf clean all

RUN dnf install -y cups hplip cups-filters python2 python2-pip gcc python2-devel cups-devel curl perl sed && \
    pip install -r /build/requirements.txt && useradd papercut -m -d /home/papercut

RUN curl -fSL "${PAPERCUT_URL}" -o /installpc.sh && \
    chmod +x /installpc.sh && /installpc.sh -e && rm /installpc.sh && \
    mkdir -p /home/papercut/providers/print/ && \
    mv papercut/providers/print/linux-x64/ /home/papercut/providers/print/linux-x64 && \
    rm -rf /papercut && \
    cd /home/papercut/providers/print/linux-x64/ && \
    chmod +x setperms roottasks && \
    ./setperms && \
    ./roottasks && \
    sed -i "s/ApplicationServer=.*/ApplicationServer=${PAPERCUT_SERVER}/" print-provider.conf

ADD config.yml /config.yml

RUN bash /build/build.sh

RUN dnf remove -y python2-pip gcc python2-devel cups-devel && \
    mv /build/start-cups.sh /start-cups.sh && chmod +x /start-cups.sh && \
    rm -rf /build/ && \
    rm /home/papercut/providers/print/linux-x64/*.pid

EXPOSE 631
CMD ["/start-cups.sh"]
